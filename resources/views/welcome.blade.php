<!doctype html>
<html lang="en">
  <head>
    <!-- Required meta tags -->
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">

    <!-- Bootstrap CSS -->
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/css/bootstrap.min.css" integrity="sha384-Gn5384xqQ1aoWXA+058RXPxPg6fy4IWvTNh0E263XmFcJlSAwiGgFAW/dAiS6JXm" crossorigin="anonymous">

    <title>Hello, world!</title>
  </head>
  <body>
    <div id="wrapper">
		<div class="container">
			<!-- Navigation -->
			<nav class="navbar navbar-default navbar-static-top" role="navigation" style="margin-bottom: 0">
				<div class="navbar-header ">
					<a class="navbar-brand" href="/">test</a>
					
				</div>
				<div class="navbar-body ">
					
					<a class="navbar-item" href="/">Refresh</a>
					
				
					
				</div>
				
				<!-- /.navbar-header -->
			</nav>

		  
		
	
  <div class="row">
    

		<table class="col-sm-12 table table-striped">
		<thead>
			<tr>
				<td>NAME</td>
				<td>PRICE</td>
				<td>CHANGE</td>
				<td>VOLUME</td>
				<td>SYMBOL</td>
			</tr>
		</thead>
		<tbody>
		<?foreach($data['stock'] as $key => $item){?>
		
			<tr>
				<td><?=$item['name']?></td>
				<td><?print_r($item['price']['amount']);?></td>
				<td><?print_r($item['percent_change']);?></td>
				<td><?print_r($item['volume']);?></td>
				<td><?print_r($item['symbol']);?></td>
			</tr>
		<? } ?>
		</tbody>
		</table>
  </div>
</div>
</div>
    <!-- Optional JavaScript -->
    <!-- jQuery first, then Popper.js, then Bootstrap JS -->
    <script src="https://code.jquery.com/jquery-3.2.1.slim.min.js" integrity="sha384-KJ3o2DKtIkvYIK3UENzmM7KCkRr/rE9/Qpg6aAZGJwFDMVNA/GpGFF93hXpG5KkN" crossorigin="anonymous"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.12.9/umd/popper.min.js" integrity="sha384-ApNbgh9B+Y1QKtv3Rn7W3mgPxhU9K/ScQsAP7hUibX39j7fakFPskvXusvfa0b4Q" crossorigin="anonymous"></script>
    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/js/bootstrap.min.js" integrity="sha384-JZR6Spejh4U02d8jOt6vLEHfe/JQGiRRSQQxSfFWpi1MquVdAyjUar5+76PVCmYl" crossorigin="anonymous"></script>
    <script src="/js/script.js" ></script>
  </body>
</html>