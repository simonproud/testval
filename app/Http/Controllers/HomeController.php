<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class HomeController extends Controller
{
    function index(){
		$data = json_decode(file_get_contents('http://phisix-api3.appspot.com/stocks.json'), true);
		return view('welcome', ['data' => $data]);
	}
}
